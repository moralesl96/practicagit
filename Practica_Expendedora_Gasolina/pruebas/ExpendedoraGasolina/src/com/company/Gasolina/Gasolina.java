package com.company.Gasolina;

public class Gasolina {

    public static final float precioPremium = 18.3f;
    public static final float precioMagna = 17.13f;
    public static final float precioDiesel = 20.13f;

    private String tipo;
    private float cantidad;
    private float precioTotal;

    public Gasolina() {
        this.tipo = null;
        this.cantidad = 0;
        this.precioTotal = 0;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getCantidad() {
        return cantidad;
    }

    public float getPrecioTipo(String tipoGas) {
        switch(tipoGas){
            case "premium":
                return precioPremium;
            case "magna":
                return precioMagna;
            case "diesel":
                return precioDiesel;
            default:
                // Apagar maquina por intento de hackeo :v
                return 99999999f;
        }
    }

    public void setPrecioTotal() {
        System.out.println(this.tipo);
        switch(this.tipo){
            case "premium":
                precioTotal = precioPremium*cantidad;
                break;
            case "magna":
                precioTotal = precioMagna*cantidad;
                break;
            case "diesel":
                precioTotal = precioDiesel*cantidad;
                break;
            default:
                // Maquina hackeada, apagar la dispensadora :v
                precioTotal = 99999999f;
                break;
        }
    }

    // Regresa la operacion total
    public float getPrecioTotal() {
        return precioTotal;
    }

}
