package com.company;

import com.company.Expendedora.Expendedora;

public class Main {
    public static boolean expendedoraAutomatica = true; // Automaticamente pedir gas despues de imprimir recibo.

    public static void main(String[] args) throws InterruptedException {


        while(expendedoraAutomatica) {
            Expendedora exp = new Expendedora();

            exp.mostrarPrecios();

            while(!exp.pedirTipoGasolina());
            while(!exp.pedirCantidadGasolina());

            exp.calcularPrecio();
            exp.confirmarGasolina();
        }
    }
}
