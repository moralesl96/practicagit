package com.company.Expendedora;
import com.company.Gasolina.Gasolina;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Expendedora {
    private static int ticket_id = 1;

    private Gasolina gas;
    private Scanner entradaTeclado = new Scanner(System.in); // Leer la entrada del teclado

    public Expendedora() {
        gas = new Gasolina();
    }

    public void mostrarPrecios()   {
        System.out.println ("==================PRECIOS DE GAS=================");
        System.out.printf ("Gasolina premium / litro: $%.2f\n", Gasolina.precioPremium);
        System.out.printf ("Gasolina magna   / litro: $%.2f\n", Gasolina.precioMagna);
        System.out.printf ("Diesel           / litro: $%.2f\n", Gasolina.precioDiesel);
        System.out.println ("=================================================");
    }

    /*
     * Leer el tipo de gasolina que desea el cliente.
     */
    public boolean pedirTipoGasolina() {

        System.out.println ("Introduzca el tipo de gasolina que desea(premium/magna/diesel): ");
        String tipoGasolina = entradaTeclado.nextLine().toLowerCase(); // Leemos el tipo

        switch (tipoGasolina) {
            case "premium":
                break;
            case "magna":
                break;
            case "diesel":
                break;
            default:
                System.out.println("Seleccionaste un tipo no valido.");
                return false;
        }
        gas.setTipo(tipoGasolina);
        return true;
    }

    /*
     * Leer el la cantidad de gasolina que desea el cliente.
     */
    public boolean pedirCantidadGasolina() {
        System.out.println ("Introduzca la cantidad de litros de gasolina que desea: ");
        String cantidadGasolina = entradaTeclado.nextLine(); // Leemos el tipo

        try {
            float cantidad = Float.parseFloat(cantidadGasolina);

            if(cantidad > 0) {
                gas.setCantidad(cantidad);
                return true;
            } else {
                throw new NumberFormatException();
            }

        } catch (NumberFormatException e) {
            System.out.println("Seleccionaste una cantidad no valida.");
            return false;
        } catch (Exception e) {
            // Reiniciar maquina por intento de hackeo :v
            return false;
        }
    }

    public void calcularPrecio() {
        gas.setPrecioTotal();
    }

    public void confirmarGasolina() throws InterruptedException {

        for (int i = 0; i < 20; i++) System.out.println(""); // Limpiar consola

        System.out.println("Tipo: " + gas.getTipo());
        System.out.println("Cantidad: " + gas.getCantidad() + " litros");
        System.out.println("Total: $" + gas.getPrecioTotal());

        System.out.println("\nIntroduzca la cantidad con la que desea pagar, o introduzca un cero y presione <<Enter>> para cancelar: ");

        boolean pagoConfirmado = false;
        while(!pagoConfirmado)
            try {
                float obtenerPago = entradaTeclado.nextFloat();

                if(obtenerPago == 0) {
                    System.out.println("Cancelaste la compra.");
                    pagoConfirmado = true;
                    break;
                }
                else if(obtenerPago >= gas.getPrecioTotal()) {
                    dispensarGasolina();

                    imprimirRecibo(obtenerPago, obtenerPago - gas.getPrecioTotal());
                    pagoConfirmado = true;
                    break;
                } else {
                    System.out.println("La cantidad con la que deseas pagar es inferior al precio total, introduce tu pago nuevamente: ");
                }
            } catch(Exception e) {
                System.out.println("Introduciste una cantidad invalida, introduzca su pago nuevamente: ");
            }
    }

    public void dispensarGasolina() throws InterruptedException {
        float litrosDispensados = 0;
        float litrosTotal = gas.getCantidad();

        while (litrosDispensados < litrosTotal) {
            for (int i = 0; i < 20; i++) System.out.println(""); // Limpiar consola
            System.out.printf("Litros: %.2f", litrosDispensados);
            litrosDispensados += 0.1;
            TimeUnit.MILLISECONDS.sleep(30);
        }
        for (int i = 0; i < 20; i++) System.out.println(""); // Limpiar consola
        System.out.println("Litros: " + litrosTotal);
    }

    public void imprimirRecibo(float pago, float cambio) { // Lo hizo el ramiro :v
        float litrosTotal = gas.getCantidad();
        Date date = new Date();
        DateFormat formatoFechaHora = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        System.out.println("*****************************************************");
        System.out.println("** Gasolinera Grupo 3");
        System.out.println("** No. " + ticket_id + ", Fecha: " + formatoFechaHora.format(date));
        System.out.println("** Maquina No. 3");
        System.out.println("** El Sauzal, Nueva Ensenada, 22880 Ensenada, B.C.");
        System.out.println("**           ");
        System.out.println("** " + litrosTotal + " LT Gasolina " + gas.getTipo() + " x $" + gas.getPrecioTipo(gas.getTipo()));
        System.out.println("**      Total =  $" + gas.getPrecioTotal());
        System.out.println("**            = -$" + pago);
        System.out.println("**          -----------");
        System.out.println("**          $" + cambio);
        System.out.println("**           ");
        System.out.println("**   Gracias por su preferencia.");
        System.out.println("**          Vuelva pronto.");
        System.out.println("*****************************************************");
        ticket_id++;
    }

}
