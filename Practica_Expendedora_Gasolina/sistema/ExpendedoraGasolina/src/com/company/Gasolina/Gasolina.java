package com.company.Gasolina;

public class Gasolina {

    public static final float precioPremium = 18.3f;
    public static final float precioMagna = 17.13f;
    public static final float precioDiesel = 20.13f;

    private String tipo;
    private static float cantidad;
    private float precioTotal;

    public Gasolina() {
        this.tipo = null;
        this.cantidad = 0;
        this.precioTotal = 0;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCantidad(float cantidad) { this.cantidad = cantidad;
    }

    public static float getCantidad() {
        return cantidad;
    }

    public float getPrecioTipo(String tipoGas) {
        switch(tipoGas){
            case "premium":
                return precioPremium;
            case "magna":
                return precioMagna;
            case "diesel":
                return precioDiesel;
            default:
                // Apagar maquina por intento de hackeo :v
                return 99999999f;
        }
    }

    public float setPrecioTotal() {
        switch(this.tipo){
            case "premium":
                     return precioPremium*cantidad;
            case "magna":
                    return precioMagna*cantidad;
            case "diesel":
                    return precioDiesel*cantidad;
            default:
                // Maquina hackeada, apagar la dispensadora :v
                return 99999999f;
        }
    }

    // Regresa la operacion total
    public float getPrecioTotal() { ;return precioTotal*cantidad;
    }

}
