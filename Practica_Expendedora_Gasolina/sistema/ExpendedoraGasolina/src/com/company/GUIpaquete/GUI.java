package com.company.GUIpaquete;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.company.Expendedora.Expendedora;
import com.company.Gasolina.Gasolina;

public class GUI extends JFrame{
    JFrame ventana = new JFrame("Expendedora");
    JButton diesel = new JButton();
    JButton premium = new JButton();
    JButton magna = new JButton();
    JLabel texto = new JLabel();
    Border border = LineBorder.createGrayLineBorder();
    public GUI(){
        initComponents();
    }

    public void initComponents(){
    ventana.setSize(700,600);
    ventana.setResizable(false);
    ventana.setVisible(true);
    ventana.setLayout(null);

    texto.setSize(560,100);
    texto.setLocation(10,10);
    texto.setBorder(border);
    texto.setText("\t Selecciona el tipo de gasolina");
    texto.setBackground(Color.WHITE);
    texto.setOpaque(true);

    diesel.setSize(200,30);
    diesel.setLocation(10,210);
    diesel.setText("Diesel $20.13");


    diesel.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            diesel.setVisible(false);
            premium.setVisible(false);
            magna.setVisible(false);
            JFrame frame = new JFrame();
            Expendedora exp = new Expendedora();
            exp.pedirTipoGasolina("diesel");
            texto.setText("\t Cuantos litros de Diesel desea");
           if(exp.pedirCantidadGasolina()){
               texto.setText("Total: " + exp.calcularPrecio() + ". Ingrese la cantidad a pagar: ");
               texto.setText("Tipo: " + exp.getTipo()+ "\n Cantidad: " + exp.getCantidad() + "\n Precio total: "+ exp.calcularPrecio()+"\n");

               exp.setPago(Float.parseFloat(JOptionPane.showInputDialog(frame, "Ingrese total a pagar: ")));

               try {
                   exp.imprimirRecibo(exp.getPago(),(exp.getPago()-exp.calcularPrecio()));
               } catch (IOException e1) {
                   e1.printStackTrace();
               }
               try {
                   TimeUnit.SECONDS.sleep(10);
               } catch (InterruptedException e1) {
                   e1.printStackTrace();
               }
               texto.setText("Gracias por su compra");
           }
       }
    });
        magna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                diesel.setVisible(false);
                premium.setVisible(false);
                magna.setVisible(false);
                JFrame frame = new JFrame();
                Expendedora exp = new Expendedora();
                exp.pedirTipoGasolina("magna");
                texto.setText("\t Cuantos litros de Magna desea");
                if(exp.pedirCantidadGasolina()){
                    texto.setText("Total: " + exp.calcularPrecio() + ". Ingrese la cantidad a pagar: ");
                    texto.setText("Tipo: " + exp.getTipo()+ "\nCantidad: " + exp.getCantidad() + "\nPrecio total: "+ exp.calcularPrecio()+"\n");

                    exp.setPago(Float.parseFloat(JOptionPane.showInputDialog(frame, "Ingrese total a pagar: ")));

                    try {
                        exp.imprimirRecibo(exp.getPago(),(exp.getPago()-exp.calcularPrecio()));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    texto.setText("Gracias por su compra");
                }
            }
        });

        premium.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                diesel.setVisible(false);
                premium.setVisible(false);
                magna.setVisible(false);
                JFrame frame = new JFrame();
                Expendedora exp = new Expendedora();
                exp.pedirTipoGasolina("premium");
                texto.setText("\t Cuantos litros de Premium desea");
                if(exp.pedirCantidadGasolina()){
                    texto.setText("Total: " + exp.calcularPrecio() + ". Ingrese la cantidad a pagar: ");
                    texto.setText("Tipo: " + exp.getTipo()+ "\nCantidad: " + exp.getCantidad() + "\n Precio total: "+ exp.calcularPrecio() +"\n");

                    exp.setPago(Float.parseFloat(JOptionPane.showInputDialog(frame, "Ingrese total a pagar: ")));

                    try {
                        exp.imprimirRecibo(exp.getPago(),(exp.getPago()-exp.calcularPrecio()));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    texto.setText("Gracias por su compra");
                }
            }
        });

    premium.setSize(200,30);
    premium.setLocation(210,210);
    premium.setText("Premium $18.3");

    magna.setSize(200,30);
    magna.setLocation(410,210);
    magna.setText("Magna $17.13");

    ventana.add(diesel);
    ventana.add(magna);
    ventana.add(premium);
    ventana.add(texto);

    }

}
