package com.company.Expendedora;
import com.company.Gasolina.Gasolina;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.lang.String;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Expendedora {
    private JFrame frame;
    private static int ticket_id = 1;
    private Gasolina gas;
    private Scanner entradaTeclado = new Scanner(System.in); // Leer la entrada del teclado
    private float pago;
    public Expendedora() {
        gas = new Gasolina();
        frame = new JFrame();
    }

    public void mostrarPrecios()   {
        System.out.println ("==================PRECIOS DE GAS=================");
        System.out.printf ("Gasolina premium / litro: $%.2f\n", Gasolina.precioPremium);
        System.out.printf ("Gasolina magna   / litro: $%.2f\n", Gasolina.precioMagna);
        System.out.printf ("Diesel           / litro: $%.2f\n", Gasolina.precioDiesel);
        System.out.println ("=================================================");
    }

    /*
     * Leer el tipo de gasolina que desea el cliente.
     */
    public boolean pedirTipoGasolina(String tipo) {

        switch (tipo) {
            case "premium":
                break;
            case "magna":
                break;
            case "diesel":
                break;
            default:
                return false;
        }
        gas.setTipo(tipo);
        return true;
    }

    /*
     * Leer el la cantidad de gasolina que desea el cliente.
     */
    public boolean pedirCantidadGasolina() {

        String cantidadGasolina = JOptionPane.showInputDialog(frame, "Cantidad de litros:");

        try {
            float cantidad = Float.parseFloat(cantidadGasolina);

            if(cantidad > 0) {
                gas.setCantidad(cantidad);
                return true;
            } else {
                throw new NumberFormatException();
            }

        } catch (NumberFormatException e) {
            System.out.println("Seleccionaste una cantidad no valida.");
            return false;
        } catch (Exception e) {
            // Reiniciar maquina por intento de hackeo :v
            return false;
        }
    }

    public float calcularPrecio() {
        return gas.setPrecioTotal();
    }

    public String getTipo() {
        return gas.getTipo();
    }
    public float getCantidad() {
        return gas.getCantidad();
    }

    public void setPago(float pago) {
        this.pago = pago;
    }

    public float getPreciototal() {
        return gas.getPrecioTotal();
    }


    public void dispensarGasolina() throws InterruptedException {
        float litrosDispensados = 0;
        float litrosTotal = gas.getCantidad();

        while (litrosDispensados < litrosTotal) {
            for (int i = 0; i < 20; i++) System.out.println(""); // Limpiar consola
            System.out.printf("Litros: %.2f", litrosDispensados);
            litrosDispensados += 0.1;
            TimeUnit.MILLISECONDS.sleep(30);
        }
        for (int i = 0; i < 20; i++) System.out.println(""); // Limpiar consola
        System.out.println("Litros: " + litrosTotal);
    }

    public float getPago() {
        return pago;
    }


    public void imprimirRecibo(float pago, float cambio) throws IOException { // Lo hizo el ramiro :v
        float litrosTotal = gas.getCantidad();
        Date date = new Date();
        DateFormat formatoFechaHora = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        /*System.out.println("*****************************************************");
        System.out.println("** Gasolinera Grupo 3");
        System.out.println("** No. " + ticket_id + ", Fecha: " + formatoFechaHora.format(date));
        System.out.println("** Maquina No. 3");
        System.out.println("** El Sauzal, Nueva Ensenada, 22880 Ensenada, B.C.");
        System.out.println("**           ");
        System.out.println("** " + litrosTotal + " LT Gasolina " + gas.getTipo() + " x $" + gas.getPrecioTipo(gas.getTipo()));
        System.out.println("**      Total =  $" + gas.setPrecioTotal());
        System.out.println("**            = -$" + pago);
        System.out.println("**          -----------");
        System.out.println("**     Cambio     $" + cambio);
        System.out.println("**           ");
        System.out.println("**   Gracias por su preferencia.");
        System.out.println("**          Vuelva pronto.");
        System.out.println("*****************************************************");*/

        String ruta = "ticket_"+ticket_id+".txt";
        File archivo = new File(ruta);
        BufferedWriter bw;

        bw = new BufferedWriter(new FileWriter(archivo));
        bw.write("*****************************************************\n");
        bw.write("*****************************************************\n");
        bw.write("** Gasolinera Grupo 3\n");
        bw.write("** No. " + ticket_id + ", Fecha: " + formatoFechaHora.format(date)+"\n");
        bw.write("** Maquina No. 3\n");
        bw.write("** El Sauzal, Nueva Ensenada, 22880 Ensenada, B.C.\n");
        bw.write("**\n");
        bw.write("** " + litrosTotal + " LT Gasolina " + gas.getTipo() + " x $" + gas.getPrecioTipo(gas.getTipo())+"\n");
        bw.write("**      Total =  $" + gas.setPrecioTotal()+"\n");
        bw.write("**            = -$" + pago+"\n");
        bw.write("**          -----------\n");
        bw.write("**     Cambio     $" + cambio+"\n");
        bw.write("**\n");
        bw.write("**   Gracias por su preferencia.\n");
        bw.write("**          Vuelva pronto.\n");
        bw.write("*****************************************************\n");

        bw.close();

        ticket_id++;


        System.out.println("Recibo generado :)");
    }

}
